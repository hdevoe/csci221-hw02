#include <iostream>
using namespace std;

long collatz_next(long n) {
    if(n % 2 == 0) {
        return (n/2);
    } else {
	return ((3*n)+1);
    }
}

long  collatz_length(int start) {
    long count = 1;
    long long  n = start;
    while(n > 1) {
        n = collatz_next(n);
        count++;
    }
    return count;
}

int main() {
    long long  max_length = 0;
    int max_start;
    long long this_length;
    
    for(long long  start = 1; start <= 1000000; start++) {
        this_length = collatz_length(start);
        if(this_length > max_length) {
            max_length = this_length;
            max_start = start;
        }
	if (start % 10000 == 0)
	{
		cout << start << ": " << this_length << endl;
	}
    }
    cout << max_start << " " << max_length << endl;
    return 0;
}

