all: ratdemo

ratdemo: main.o rational.o
	g++ -o ratdemo main.o rational.o

main.o: main.cpp
	g++ -c main.cpp

rational.o: rational.cpp
	g++ -c rational.cpp

clean: 
	rm -f *.o  ratdemo

